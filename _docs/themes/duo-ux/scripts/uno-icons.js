var fs = require('hexo-fs');

/**
 * Reads the version from the main package.json file
 */
hexo.extend.helper.register('listDir', function(path){
  var files = fs.listDirSync(path);

  return files;
});
