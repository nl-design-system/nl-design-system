var fs = require('hexo-fs');

/**
 * Reads the version from the main package.json file
 */
hexo.extend.helper.register('version', function(){
  var file = fs.readFileSync('package.json'),
    pkg = JSON.parse(file);

  return pkg.version;
});
