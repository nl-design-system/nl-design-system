---
layout: content-page
permalink: changelog/
title: Changelog
---
<a name="0.1.1"></a>
## 0.1.1 (2020-10-26)


### Bug Fixes

* make links in docs relative ([62d9da0](https://gitlab.com/nl-design-system/nl-design-system/commit/62d9da0))
* rename NLDS to NL Design System ([824b73f](https://gitlab.com/nl-design-system/nl-design-system/commit/824b73f))
* revert root URL as the docs contain absolute URLs, this setting makes the docs unusable ([980c95c](https://gitlab.com/nl-design-system/nl-design-system/commit/980c95c))
* use public GitLab url in footer ([eb29765](https://gitlab.com/nl-design-system/nl-design-system/commit/eb29765))



