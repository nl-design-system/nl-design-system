module.exports = (grunt) => {
  "use strict";
  const terminal = require('child_process').exec;

  // Builds a new version of the library. Because the a11y tests are running against compiled code,
  // We need to build before we can run action tests
  grunt.registerTask('changelog', 'Build a fresh version of the library', [
    'conventionalChangelog:generate',
    'copy:changelog',
    'usebanner:changelog',
    'addChangelog'
  ]);

  grunt.registerTask('addChangelog', 'Commits changelog files', function() {
    var done = this.async();
    terminal('git add HISTORY.MD _docs/source/changelog.md', (err) => {
      if(err) {
        console.log('Could not add changelog to git');
      }
      done();
    })
  });

};