module.exports = {
  // Copies all the asset from the src folder to the dist folder
  dist: {
    files: [
      {
        expand: true,
        cwd: 'dist',
        src: ['**/*.js'],
        dest: 'dist/',
        rename: function (dst, src) {
          return dst + '/' + src.replace('.js', '.min.js');

        }
      }
    ]
  }
};
