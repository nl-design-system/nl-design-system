tilde_importer = require('grunt-sass-tilde-importer');

module.exports = {

  options: {
    sourceMap: true,
    importer: tilde_importer,
    outputStyle: 'expanded'
  },

  // Dev generates sourcemaps and doesn't minify output
  dev: {
    files: [
      {
        expand: true,
        cwd: 'src',
        src: ['**/*.scss'],
        // Output to _docs folder instead of docs, so Hexo's browsersync will pick up changes
        dest: '_docs/themes/duo-ux/source/uno',
        ext: '.css'
      }
    ]
  },

  // Dist does not generate sourcemaps and minifies output
  dist: {
    options: {
      sourceMap: '.tmp/sourcemap',
      sourceMapEmbed: false,
      outputStyle: 'compressed'
    },
    files: [
      {
        expand: true,
        cwd: 'src',
        src: ['**/*.scss'],
        dest: 'dist',
        ext: '.css'
      }
    ]
  }
};
