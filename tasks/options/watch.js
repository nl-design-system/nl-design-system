module.exports = {
    styles: {
        // Watches for stylesheet changes, copies asset (because sass empties
        // output dir), lints the css and then processes the sass files
        files: 'src/**/*.scss',
        tasks: ['postcss:lint', 'sass:dev', 'postcss:dev', 'copy:dev'],
        options: {
            debounceDelay: 250,
            spawn: false,
            interrupt: true
        }
    },
    typescript: {
        // Watches for changes in asset files and triggers copy to dev
        files: ['src/**/*.ts'],
        tasks: ['tslint'],
        options: {
            debounceDelay: 250,
            spawn: false,
            interrupt: true
        }
    },
    assets: {
        // Watches for changes in asset files and triggers copy to dev
        files: ['src/**', '!src/**/*.scss', '!src/**/*.ts', '!src/vendor/**/*.js'],
        tasks: ['copy:dev'],
        options: {
            debounceDelay: 250,
            spawn: false,
            interrupt: true
        }
    },
    devstyles: {
        // Watches for stylesheet changes, copies asset (because sass empties
        // output dir), lints the css and then processes the sass files
        files: 'src/**/*.scss',
        tasks: ['clean:dist', 'typescript:dist', 'postcss:lint', 'sass:dist', 'postcss:dist', 'copy:dist', 'concat:dist'],
        options: {
            debounceDelay: 250,
            interrupt: true
        }
    },
    devtypescript: {
        // Watches for changes in asset files and triggers copy to dev
        files: ['src/**/*.ts'],
        tasks: ['tslint'],
        options: {
            debounceDelay: 250,
            interrupt: true
        }
    },
    devassets: {
        // Watches for changes in asset files and triggers copy to dev
        files: ['src/**', '!src/**/*.scss', '!src/**/*.ts', '!src/vendor/**/*.js'],
        tasks: ['copy:dist'],
        options: {
            debounceDelay: 250,
            interrupt: true
        }
    }
};
