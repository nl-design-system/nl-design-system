module.exports = (grunt) => {
    'use strict';
  
    grunt.registerTask('theme', 'Set theme', [
      'clean:theme',
      'copy:theme'
    ]);
  };
  