// module.exports = (grunt) => {
//   "use strict";
//
//   const
//     backstop = require('backstopjs'),
//     cwd = process.cwd(),
//     http = require('http'),
//     fs = require('fs'),
//     tar = require('tar-fs');
//
//   let options =
//     {
//       referenceUrl: 'http://dev.idsklijnsma.nl/duo/reference.tar',
//       configFile: `${cwd}/config/backstop.json`
//     };
//
//   // Builds a new version of the library. Because the a11y tests are
//   // running against compiled code,
//   // We need to build before we can run action tests
//   grunt.registerTask('visual:generate', 'Generate Reference images', generate);
//   grunt.registerTask('visual:reference', 'visual:generate');
//   grunt.registerTask('visual:test', 'Run visual tests', test);
//   grunt.registerTask('visual:download', 'Download reference images', download);
//
//   /**
//    * Downloads the reference images from an external url.
//    */
//   function download() {
//     let
//       done = this.async(),
//       dest = `${cwd}/backstop_data/bitmaps_reference/`;
//
//     // Download the images
//     http.get(options.referenceUrl, (response) => {
//       // Save it
//       console.log(`Successfully downloaded ${options.referenceUrl}`);
//
//       response
//         .pipe(tar.extract(dest))
//
//         .on('finish', () => {
//           console.info('Unzipped all files');
//           done();
//         })
//
//         .on('error', (err) => {
//           console.error('Error reading tar file');
//           done(err.message);
//         });
//
//     }).on('error', function (err) { // Handle errors
//       done(err.message)
//     });
//
//   }
//
//   /**
//    * Generates new reference images by running the npm task from the backstopjs
//    * folder
//    */
//   function generate() {
//     let
//       done = this.async(),
//       dir = `${cwd}/backstop_data/`;
//
//     backstop('reference', {
//       config: require(options.configFile)
//     })
//       .then(() => {
//         tar
//           .pack(`${dir}/bitmaps_reference`)
//           .pipe(fs.createWriteStream(`${dir}/reference.tar`))
//           .on('end', err => {
//             if (err) {
//               grunt.task.warn('Could not generate tarball');
//             }
//             done(err);
//           });
//
//       });
//   }
//
//   /**
//    * Runs the visual regression tests by checking the reference images against
//    * the current output. Runs the npm task from the backstopjs folder
//    */
//   function test() {
//     let done = this.async();
//     console.log('Running visual regression tests');
//
//     let files = fs.readdirSync(`${cwd}/backstop_data/bitmaps_reference/`),
//       hasImages = false;
//
//     // Check if there are images in the reference folder
//     for (let file of files) {
//       if (file.endsWith('.png')) {
//         hasImages = true;
//         break;
//       }
//     }
//
//     if (!hasImages) {
//       grunt.fail.fatal(
//         `Reference folder contains 0 images,
//         generate images or download reference images,
//         run grunt visual:download
//         or grunt visual:generate`);
//     }
//
//     backstop('test',{
//       config: require(options.configFile)
//     }).then(done);
//
//   }
//
// };