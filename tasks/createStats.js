'use strict';

module.exports = (grunt) => {

  grunt.registerTask('stats', 'Generate stats for autoprefixer', () => {
    let stats = grunt.file.readJSON('./config/stats.json');
    let total = 0;
    /**
     * Contains the percentual stats in the following format:
     * {
     *  ie: {
     *    "8.0": 0.02,
     *    "9.0": 5.83
     *  },
     *  chrome: {
     *   "55.0": 12.0
     *  },
     *  ...
     * @type {{}}
     */
    let browsers = {};
    let browsernames = {
      FF: 'Firefox',
      CM: 'and_uc',
      CH: 'chrome',
      MF: 'ios_saf',
      CI: 'Chrome Mobile iOS',
      CR: 'Chrome',
      SF: 'Safari',
      IE: 'ie',
      PS: 'Edge',
      OP: 'Opera',
      IM: 'ie_mob',
      AN: 'Android'
    };
    let regex = /browserCode==([^;]*);browserVersion==(.*)/;

    Array.from(stats).forEach(browser => {
      total += browser.nb_uniq_visitors;
    });

    Array.from(stats).forEach(stat => {
      let browser = stat.segment.split(regex);
      setStat(browser[1], browser[2], stat.nb_uniq_visitors);
    });

    /**
     * Sets the percentual visits for each browser version,
     * @param code
     * @param version
     * @param visits
     */
    function setStat(code, version, visits) {
      if(browsernames.hasOwnProperty(code)) {
        let browser = browsernames[code];
        if(!browsers.hasOwnProperty(browser)) {
          browsers[browser] = {};
        }

        browsers[browser][version] = (visits / total) * 100;
      }
    }
    grunt.file.write('config/browserlist-stats.json', JSON.stringify(browsers));
  });

};