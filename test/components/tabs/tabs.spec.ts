import {} from 'jasmine';
import {Tabs} from '../../../src/components/tabs/tabs';
import {Utils} from '../../../src/core/utils';

const TEMPLATE: string = `
<div class="test-holder">
    <ul class="tab" role="tablist">
      <li id="tab1" role="tab" aria-controls="panel1" class="tab__tab"><a href="#panel1">Tab 1</a></li>
      <li id="tab2" role="tab" aria-controls="panel2" class="tab__tab"><a href="#panel2">Tab 2</a></li>
      <li id="tab3" role="tab" aria-controls="panel3" class="tab__tab"><a href="#panel3">Tab 3</a></li>
    </ul>

    <div class="tab__content">
      <div class="tab__pane" id="panel1" role="tabpanel" aria-labelledby="tab1">
          <p>Tab 3</p>
      </div>
      <div class="tab__pane" id="panel2" role="tabpanel" aria-labelledby="tab2">
          <p>Tab 2</p>
      </div>
      <div class="tab__pane" id="panel3" role="tabpanel" aria-labelledby="tab3">
          <p>Tab 3</p>
      </div>
    </div>
</div>`;

const TEMPLATE_DOM_STRUCTURE: string = `
<div class="test-holder">
    <ul class="tab" role="tablist">
      <li id="tab1" role="tab" aria-controls="panel1" class="tab__tab"><a href="#panel1">Tab 1</a></li>
      <li id="tab2" role="tab" aria-controls="panel2" class="tab__tab"><a href="#panel2">Tab 2</a></li>
      <li id="tab3" role="tab" aria-controls="panel3" class="tab__tab"><a href="#panel3">Tab 3</a></li>
    </ul>

    <div class="content-background tab__content">
        <div class="should-not-matter">
          <div class="content tab__pane" id="panel1" role="tabpanel" aria-labelledby="tab1">
              <p>Tab 3</p>
          </div>
        </div>

       <div class="should-not-matter">
          <div class="content tab__pane" id="panel2" role="tabpanel" aria-labelledby="tab2">
              <p>Tab 2</p>
          </div>
      </div>

      <div class="should-not-matter">
          <div class="content tab__pane" id="panel3" role="tabpanel" aria-labelledby="tab3">
              <p>Tab 3</p>
          </div>
      </div>
    </div>
</div>`;

describe('Tabs component', () => {

    beforeEach(() => {
        if (window.top['callPhantom']) {
            // PhantomJS does not support original implementation
            spyOn(Utils, 'CreateNode')
                .and
                .callFake((html:string) => new DOMParser().parseFromString(html, 'text/xml').firstChild as HTMLElement);
        }
    });

    function getTemplate(tpl: string = null): HTMLElement {
        if (!tpl) {
            tpl = TEMPLATE;
        }
        const el: Element = document.createElement('div');
        el.innerHTML = tpl;
        return el.firstElementChild as HTMLElement;
    }

    it('should check if an element is passed to the constructor', () => {
        // Setup
        let thrown: string = '';

        // Execute
        try {
            new Tabs(null);
        } catch (e) {
            thrown = e.message;
        }

        // Test
        expect(thrown).toBe('Host element not supplied');
    });

    it('should check if the element passed to the constructor contains tabs', () => {
        // Setup
        const element: Element = document.createElement('div');
        let thrown: string = '';

        // Execute
        try {
            new Tabs(element);
        } catch (e) {
            thrown = e.message;
        }

        // Test
        expect(thrown).toBe('No tabs found in host element');
    });

    it('should set the first tab to `active` when there is no active tab', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);

        // Execute
        new Tabs(element);

        // Test
        let hasClass: boolean = (element.querySelector('#tab1') as Element).classList.contains('tab__tab--active');
        expect(hasClass).toBe(true);
    });

    it('should not set the first tab to `active` when there is an active tab', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);

        (element.querySelector('#tab2') as Element).classList.add('tab__tab--active');

        // Execute
        new Tabs(element);

        // Test
        const hasClass: boolean = (element.querySelector('#tab1') as Element).classList.contains('tab__tab--active');
        expect(hasClass).toBe(false);
    });

    it('should activate the corresponding pane when a tab is clicked', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        new Tabs(element);

        // Execute
        (element.querySelector('#tab2 a') as HTMLElement).click();
        // Test
        const isActiveTab: boolean = (element.querySelector('#tab2') as Element).classList.contains('tab__tab--active');
        const isActivePane: boolean = (document.querySelector('#panel2') as Element).classList.contains('tab__pane--active');
        expect(isActiveTab).toBe(true);
        expect(isActivePane).toBe(true);
    });

    it('should apply aria attributes when a tab is clicked', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        new Tabs(element);

        // Execute
        (element.querySelector('#tab2 a') as HTMLElement).click();
        // Test
        const ariaSelectedValue: string = (element.querySelector('#tab2') as Element).getAttribute('aria-selected');
        expect(ariaSelectedValue).toBe('true');
    });

    it('should have only 1 active tab', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        new Tabs(element);

        // Execute
        (element.querySelector('#tab2 a') as HTMLElement).click();
        // Test
        const tabActive: NodeList = document.querySelectorAll('.tab__tab--active:not(.tab__tab--clone)');
        expect(tabActive.length).toBe(1);
    });

    it('should activate the correct tab', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        const tabs: Tabs = new Tabs(element);
        const activeTab: Element = document.getElementById('panel3');

        // Execute
        tabs.openTab(2);

        // Test
        expect(activeTab.classList.contains('tab__pane--active')).toBe(true);
    });

    it('should have only 1 active pane', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        new Tabs(element);

        // Execute
        (element.querySelector('#tab2 a') as HTMLElement).click();
        // Test
        const paneActive: NodeList = document.querySelectorAll('.tab__pane--active');
        expect(paneActive.length).toBe(1);
    });

    it('should have only 1 active tab set with aria attributes', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        new Tabs(element);

        // Execute
        (element.querySelector('#tab2 a') as HTMLElement).click();
        // Test
        const ariaSelected: NodeList = document.querySelectorAll('[aria-selected="true"]:not(.tab__tab--clone)');

        expect(ariaSelected.length).toBe(1);
    });

    it('should have only 1 active pane set with aria attributes', () => {
        // Setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);
        new Tabs(element);

        // Execute
        (element.querySelector('#tab2 a') as HTMLElement).click();
        // Test
        const paneActive: NodeList = document.querySelectorAll('.tab__pane--active');
        expect(paneActive.length).toBe(1);
    });

    it('should return the element targeted by the href', () => {
        // Setup
        const STRUCTURE: string = `
            <div class="test-holder">
                <div class="tab__tab" id="result">
                    <div>
                        <div>
                            <a href="#result"></a>
                        </div>
                    </div>
                </div>
            </div>`;
        const el: HTMLElement = getTemplate(STRUCTURE);
        document.body.appendChild(el);

        // Execute
        const result: Element = Tabs.getTargetPane(el.querySelector('a'));

        // Test
        expect(result.getAttribute('id')).toBe('result');
    });

    it('should deactivate all tabs', () => {
        const STRUCTURE: string = `
            <div class="test-holder">
                <ul class="tab" role="tablist">
                  <li id="tab1" role="tab" aria-controls="panel1" class="tab__tab tab__tab--active"><a href="#panel1">Tab 1</a></li>
                  <li id="tab2" role="tab" aria-controls="panel2" class="tab__tab"><a href="#panel2">Tab 2</a></li>
                  <li id="tab3" role="tab" aria-controls="panel3" class="tab__tab tab__tab--active"><a href="#panel3">Tab 3</a></li>
                </ul>
            </div>`;

        const el: HTMLElement = getTemplate(STRUCTURE);
        document.body.appendChild(el);

        expect(document.querySelectorAll('.tab__tab--active').length).toBe(2);

        Tabs.deactivateTabs(el);

        expect(document.querySelectorAll('.tab__tab--active').length).toBe(0);
        expect(document.querySelectorAll('[aria-selected="false"]').length).toBe(2);
    });

    it('should deactivate all panes', () => {
        // setup
        const element: HTMLElement = getTemplate();
        document.body.appendChild(element);

        new Tabs(element);

        expect(document.querySelectorAll('.tab__pane--active').length).toBe(1);

        Tabs.deactivatePanes(element);

        expect(document.querySelectorAll('.tab__pane--active').length).toBe(0);
    });

    describe('Tabs component different DOM structure', () => {

        it('should not matter if the tab__panes are surrounded by another element', () => {
            const element: HTMLElement = getTemplate(TEMPLATE_DOM_STRUCTURE);
            document.body.appendChild(element);
            new Tabs(element);

            expect(document.querySelectorAll('.tab__pane--active').length).toBe(1);

            // Execute
            (element.querySelector('#tab2 a') as HTMLElement).click();
            expect(document.querySelectorAll('.tab__pane--active').length).toBe(1);

            (element.querySelector('#tab3 a') as HTMLElement).click();
            expect(document.querySelectorAll('.tab__pane--active').length).toBe(1);

            (element.querySelector('#tab1 a') as HTMLElement).click();
            expect(document.querySelectorAll('.tab__pane--active').length).toBe(1);

        });

    });

    afterEach(() => {
        const holder: Element = document.querySelector('.test-holder');

        if (holder && holder.parentElement) {
            holder.parentElement.removeChild(holder);
        }
    });

});
