import { AutoResizeNavigation } from '../../../src/components/navigation/navigation';
import 'jasmine';

const PRE_TEMPLATE :string = `
     <h1>div preceding sibling autoresize list</h1>
     <ul>
        <li>
            <p>containing stuff</p>
        </li>
     </ul>
`;
const TEMPLATE :string = `
    <ul class="nav">
        <li class="nav__item" id="item1">
            <a class="nav__link " href="#">link1</a>
        </li>
        <li class="nav__item" id="item2">
            <a class="nav__link " href="#">link2</a>
        </li>
        <li class="nav__item" id="item3">
            <a class="nav__link " href="#">link3</a>
        </li>
    </ul>
`;
const POST_TEMPLATE :string = `
    <h1>div succeeding sibling autoresize list</h1>
     <ul>
        <li>
            <p>containing stuff</p>
        </li>
     </ul>
`;

describe('Auto-resizing-navigation component', ()=>{

    it('should check if element is passed to constructor', () => {
        let element:Element = document.createElement('div');
        let thrown:string = '';

        try{
            new AutoResizeNavigation(null);
        } catch (e){
            thrown = e.message;
        }
        expect(thrown).toBe('host element not defined');
    });

    it('should check if element passed to constructor has right class', () => {
        let element:Element = document.createElement('div');
        let thrown:string = '';

        try{
            new AutoResizeNavigation(element);
        } catch (e){
            thrown = e.message;
        }
        expect(thrown).toBe('host element should have class top-nav-autoresize');
    });

    it('should check if element passed to constructor is in container', () => {

        let outerElement:Element = document.createElement('div');

        outerElement.classList.add('top-nav-autoresize');

        document.body.appendChild(outerElement);

        let thrown:string = '';

        try{
            new AutoResizeNavigation(outerElement);
        } catch(e){
            thrown = e.message;
        }
    });

    it('should check if element passed to constructor is in container', () => {

        let outerElement:Element = document.createElement('div');
        let innerElement:Element = document.createElement('div');

        innerElement.classList.add('top-nav-autoresize');
        outerElement.classList.add('container');
        innerElement.innerHTML = '<ul><li>bla</li></ul>';

        outerElement.appendChild(innerElement);
        document.body.appendChild(outerElement);

        let thrown:string = '';

        try{
            new AutoResizeNavigation(innerElement);
        } catch(e){
            thrown = e.message;

        }
        expect(thrown).toBe('host element should have child ul with class nav');
    });

    it('should add flex classes ', () => {
        let container:Element = document.createElement('div');
        container.classList.add('container');
        let preElement:Element = document.createElement('div');
        let postElement:Element = document.createElement('div');
        let navigationElement:Element = document.createElement('div');
        navigationElement.classList.add('top-nav-autoresize');
        preElement.innerHTML = PRE_TEMPLATE;
        postElement.innerHTML = POST_TEMPLATE;
        navigationElement.innerHTML = TEMPLATE;

        container.appendChild(preElement);
        container.appendChild(navigationElement);
        container.appendChild(postElement);
        document.body.appendChild(container);

        new AutoResizeNavigation(navigationElement);
        // expect(container.classList).toContain('container--flex', `Contents: [${container.classList.toString()}]`);
        expect(postElement.classList).toContain('autoresize__sibling');
        expect(preElement.classList).toContain('autoresize__sibling');
    });

});
